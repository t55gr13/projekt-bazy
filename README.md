# Pizzeria app
Simple app for managing process of production in restaurant

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Usage](#usage)

## General info
Aplikacja została stworzona na potrzeby wykonania projektu z przedmiotu "Bazy Danych".\
...

## Technologies
- Python 3
- PyQt5 5.15.2
- mysql-connector-python 8.0.23
- MySQL 8

## Setup
To use or install application, MySQL Server 8 and Python 3.7 or higher must be installed on PC.
### Windows
Download or clone repository:\
`git clone https://gitlab.com/tgruca/projekt-bazy.git`

Create virtual environment and activate it:\
`python -m venv .`\
`Scripts\activate`

To initialize database, run `db_init.bat <login> <password>`, where login and password are mysql credentials for root user.\
Next, run `setup.bat` script to install required modules and create executable in `dist/` directory.

### Linux
(Tested on Ubuntu 20.04)\

Download or clone repository:\
`git clone https://gitlab.com/tgruca/projekt-bazy.git`

Create virtual environment and activate it:\
`python3 -m venv .`\
`./bin/activate`

To initialize database, run `sudo ./db_init.sh`.\
Next, run `./setup.sh` script to install required modules and create executable in `dist/` directory.
> You have to modify file permissions to run setup script\
> `chmod 744 setup.sh`

## Usage
After executing setup script, you can run application from console:\
`./pizzeria.py`

You can also run executable from dist/ directory.
