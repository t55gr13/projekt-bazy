import sys
import PyQt5.QtWidgets as qt
import ui
import ctrl
import database

from os import environ

def suppress_qt_warnings():
    environ["QT_DEVICE_PIXEL_RATIO"] = "0"
    environ["QT_AUTO_SCREEN_SCALE_FACTOR"] = "1"
    environ["QT_SCREEN_SCALE_FACTORS"] = "1"
    environ["QT_SCALE_FACTOR"] = "1"

class App(object):
    """Main application object.
    Responsible for doing all logic and calculations, 
    controlling app and communicating with database.
    """
    suppress_qt_warnings()
    _qt_app = qt.QApplication(sys.argv)

    def __init__(self, settings):
        """App object constructor
        """
        print("[D] Creating App object")
        # Create window and configure it
        self._settings = settings

        # Setup app window
        # TODO: make window an object (?)
        self._window = qt.QMainWindow()
        self._window.setWindowTitle(settings['window']['title'])
        self._window.resize(settings['window']['width'], settings['window']['height'])

        # Create database connection
        self._db = database.Database(settings['db'])
        if self._db.connect() is False:
            raise RuntimeError('\nFailed to connect to database.\nCheck connection parameters in \'app.config\'')

        # Create variable for user
        self._user = None

    def run(self):
        """Run application
        """
        print("[D] App is now running...")
        self._qt_app.exec_()
        print("[D] App stopped running")

    def setUI(self, name):
        """Set current user interface

        Args:
            name (string): name of UI
        """
        # Create new UI and link controller to it
        # TODO: use enums instead of strings
        if name == 'login':
            self.ui = ui.OknoLogowania()
            self.ctrl = ctrl.LoginCtrl(self, self.ui)
        elif name == 'main':
            self.ui = ui.Main()
            self.ctrl = ctrl.MainCtrl(self, self.ui)
        elif name == 'main_menu':
            self.ui = ui.OknoMainMenu()
            self.ctrl = ctrl.MainMenuCtrl(self, self.ui)
        elif name == 'menu':
            self.ui = ui.OknoMenu(self._settings['ui'])
            self.ctrl = ctrl.MenuCtrl(self, self.ui)
        elif name == 'produkty':
            self.ui = ui.OknoProdukty()
            self.ctrl = ctrl.ProduktyCtrl(self, self.ui)
        elif name == 'zamowienie':
            self.ui = ui.OknoZamowienia()
            self.ctrl = ctrl.ZamowienieCtrl(self, self.ui)
        else:
            # No correct UI name given
            raise RuntimeError('no ui name found')
        

        # Set UI to the main window
        self._window.setCentralWidget(self.ui)
        print("[D] Set UI to '{}'".format(name))


    def checkPassword(self, user, password):
        """Check user credentials.

        Args:
            user (string): user login
            password (string): user password

        Returns:
            bool: True if credentials are ok, false if not
        """
        # Query user from database
        print("[D] Trying to log into database")

        q = self._db.query("select * from pracownicy where login='{}' and haslo='{}'".format(user, password))
        # If only 1 user is found, creds are ok
        if len(q) == 1:
            print("[D] Logged in as '{}'".format(user))
            return True
        else:
            print("[D] Failed to log in as '{}'".format(user))
            return False


    def show(self):
        """Show window on screen
        """
        self._window.show()
