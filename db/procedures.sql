############################
# PROCEDURY
############################

# aktualizuj stan skladnikow
-- DROP PROCEDURE IF EXISTS aktualizuj_skladniki;
DROP PROCEDURE IF EXISTS zrealizuj_zamowienie;
DELIMITER $$
CREATE PROCEDURE zrealizuj_zamowienie(
	IN id_zam INT UNSIGNED,
	IN platnosc INT UNSIGNED
)
BEGIN
	DECLARE done INT;

	DECLARE id_prod INT UNSIGNED;
	DECLARE lb INT UNSIGNED;
  
	DECLARE curs CURSOR FOR SELECT idproduktu, liczba FROM zawartosczamowienia WHERE idzamowienia=id_zam;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

	# check if order is done already
	# nie mozna aktualizowac skladnikow wiecej niz 1 raz
	IF czy_zrealizowane(id_zam) THEN
		SIGNAL SQLSTATE '45000' 
		SET MESSAGE_TEXT = 'Zamowienie jest juz zrealizowane';
	END IF;
  
	# Set timestamp and payment type
	UPDATE zamowienia SET czasplatnosci=CURRENT_TIMESTAMP(), 
	idtypu=platnosc WHERE idzamowienia=id_zam;

	# iterate over products in order
	OPEN curs;  
	SET done = 0;
	REPEAT
		FETCH curs INTO id_prod,lb;
		IF (done=0) THEN
			CALL aktualizuj_stan_skladnikow(id_prod, lb);
		END IF;
	UNTIL done END REPEAT;  
	CLOSE curs;
END$$
DELIMITER ;



DROP PROCEDURE IF EXISTS aktualizuj_stan_skladnikow;
DELIMITER $$
CREATE PROCEDURE aktualizuj_stan_skladnikow(
	IN id_prod INT UNSIGNED,
	IN ilosc INT UNSIGNED
)
BEGIN
	DECLARE done INT;

	DECLARE id_skl INT UNSIGNED;
	DECLARE lb FLOAT(10);
  
	DECLARE curs CURSOR FOR SELECT idskladnika, liczba FROM skladproduktu WHERE idproduktu=id_prod;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

	# iterate over ingredients
	OPEN curs;
	SET done = 0;
	REPEAT
		FETCH curs INTO id_skl,lb;
		IF (done=0) THEN
			# save change in ingredient amount
			UPDATE skladniki SET suma=suma-lb*ilosc WHERE idskladnika=id_skl;
		END IF;
	UNTIL done END REPEAT;
	CLOSE curs;
END$$
DELIMITER ;



# Sprawdzenie czy zamowienie zostalo juz zrealizowane
DROP FUNCTION IF EXISTS czy_zrealizowane;
DELIMITER $$
CREATE FUNCTION czy_zrealizowane(id INT UNSIGNED)
	RETURNS BOOL
	DETERMINISTIC
BEGIN
	IF (SELECT czasplatnosci FROM zamowienia WHERE 
		idzamowienia=id) IS NOT NULL THEN
		RETURN (true);
	ELSE
		RETURN (false);
	END IF;
END$$
DELIMITER ;