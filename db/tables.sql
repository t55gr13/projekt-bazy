############################
# TABELE
############################

DROP TABLE IF EXISTS produkty CASCADE;
CREATE TABLE produkty
(
idproduktu INT UNSIGNED NOT NULL AUTO_INCREMENT,
PRIMARY KEY (idproduktu),
nazwa VARCHAR(255),
cena DECIMAL(6,2),
idtypu INT UNSIGNED NOT NULL
);

DROP TABLE IF EXISTS skladniki cascade;
CREATE table skladniki
(
idskladnika INT UNSIGNED NOT NULL AUTO_INCREMENT,
PRIMARY KEY (idskladnika),
nazwa VARCHAR(255),
suma FLOAT(10),
jednostka VARCHAR(10),
poziomminimalny FLOAT(10)
);

DROP TABLE IF EXISTS skladproduktu CASCADE;
CREATE TABLE skladproduktu
(
idproduktu INT UNSIGNED NOT NULL,
idskladnika INT UNSIGNED NOT NULL,
liczba FLOAT(1) NOT NULL
);

DROP TABLE IF EXISTS typproduktu CASCADE;
CREATE TABLE typproduktu
(
idtypu INT UNSIGNED NOT NULL AUTO_INCREMENT,
PRIMARY KEY (idtypu),
nazwatypu VARCHAR(255)
);

DROP TABLE IF EXISTS zawartosczamowienia CASCADE;
CREATE TABLE zawartosczamowienia
(
idzamowienia INT UNSIGNED NOT NULL,
idproduktu INT UNSIGNED NOT NULL,
liczba INT NOT NULL
);

DROP TABLE IF EXISTS zamowienia CASCADE;
CREATE TABLE zamowienia 
(
idzamowienia INT UNSIGNED NOT NULL AUTO_INCREMENT,
PRIMARY KEY (idzamowienia),
czaszlozenia TIMESTAMP,
czasplatnosci TIMESTAMP,
idtypu INT UNSIGNED DEFAULT NULL,
wartoscrachunku DECIMAL(6,2) DEFAULT 0.0,
idpracownika INT UNSIGNED NOT NULL
);

DROP TABLE IF EXISTS pracownicy CASCADE;
CREATE TABLE pracownicy
(
idpracownika INT UNSIGNED NOT NULL AUTO_INCREMENT,
PRIMARY KEY (idpracownika),
imie VARCHAR(20),
nazwisko VARCHAR(40),
numertelefonu INT,
kierownik BIT,
login VARCHAR(30),
haslo VARCHAR(30)
);

DROP TABLE IF EXISTS typplatnosci CASCADE;
CREATE TABLE typplatnosci
(
idtypu INT UNSIGNED NOT NULL AUTO_INCREMENT,
PRIMARY KEY (idtypu),
nazwaplatnosci VARCHAR(64) NOT NULL
);

##########################
# KLUCZE OBCE
##########################
ALTER TABLE produkty ADD FOREIGN KEY (idtypu) REFERENCES typproduktu(idtypu)
	ON UPDATE CASCADE
	ON DELETE CASCADE;
ALTER TABLE skladproduktu ADD FOREIGN KEY (idproduktu) REFERENCES produkty(idproduktu)
	ON UPDATE CASCADE
	ON DELETE CASCADE;
ALTER TABLE skladproduktu ADD FOREIGN KEY (idskladnika) REFERENCES skladniki(idskladnika)
	ON UPDATE CASCADE
	ON DELETE CASCADE;
ALTER TABLE zawartosczamowienia ADD FOREIGN KEY (idzamowienia) REFERENCES zamowienia(idzamowienia)
	ON UPDATE CASCADE
	ON DELETE CASCADE;
ALTER TABLE zawartosczamowienia ADD FOREIGN KEY (idproduktu) REFERENCES produkty(idproduktu)
	ON UPDATE CASCADE
	ON DELETE CASCADE;
ALTER TABLE zamowienia ADD FOREIGN KEY (idtypu) REFERENCES typplatnosci(idtypu)
	ON UPDATE CASCADE
	ON DELETE CASCADE;
ALTER TABLE zamowienia ADD FOREIGN KEY (idpracownika) REFERENCES pracownicy(idpracownika)
	ON UPDATE CASCADE
	ON DELETE CASCADE;



##########################
# WIĘZY
##########################
ALTER TABLE produkty ADD CHECK (cena>0);
ALTER TABLE zamowienia ADD CHECK (wartoscrachunku>=0);
-- ALTER TABLE zamowienia ADD CHECK (czaszlozenia<czasplatnosci);
ALTER TABLE pracownicy ADD CHECK (length(numertelefonu) IN (9));

##########################
# WIDOKI
##########################

# widok pokazujący które składniki są poniżej dopuszczonego stanu
DROP VIEW IF EXISTS stan_skladnika CASCADE;
CREATE VIEW stan_skladnika 
AS SELECT idskladnika, nazwa, suma 
FROM skladniki 
where suma < poziomminimalny;


# widok pokazujacy zamowienia o najwiekszej wartosci i ktory pracownik 
# je obslugiwal
DROP VIEW IF EXISTS najlepsze_transakcje CASCADE;
CREATE VIEW najlepsze_transakcje 
AS SELECT idpracownika, imie, nazwisko, wartoscrachunku
FROM zamowienia 
inner JOIN pracownicy USING (idpracownika)
-- WHERE zamowienia.wartoscrachunku>150;
ORDER BY wartoscrachunku DESC
LIMIT 5;

# widok pokazujacy jakie produkty naleza do danego typu
DROP VIEW IF EXISTS typy_produktow CASCADE;
CREATE VIEW typy_produktow
AS SELECT idtypu, idproduktu, nazwa, cena 
FROM produkty INNER JOIN typproduktu USING (idtypu);


# zamówienia utworzone danego dnia
DROP VIEW IF EXISTS zamowienia_dzisiaj CASCADE;
CREATE VIEW zamowienia_dzisiaj
AS SELECT idzamowienia, czaszlozenia, czasplatnosci, idtypu, wartoscrachunku, idpracownika
FROM zamowienia
WHERE DATE(czaszlozenia) >= CURRENT_DATE()
