from PyQt5.QtWidgets import (
    QApplication, QWidget, QPushButton, QLabel, QLineEdit, QGridLayout, QMessageBox)
from PyQt5.QtGui import *
from PyQt5.QtCore import *


class OknoLogowania(QWidget):
    def __init__(self):
        super().__init__()

        # Create UI layout and apply it
        layout = QGridLayout()
        layout.addWidget(self._createLoginArea())
        self.setLayout(layout)

    def _createLoginArea(self):

        loginArea = QWidget()
        loginArea.setFixedSize(450, 220)
        loginArea.setObjectName('login-area')
        loginArea.setStyleSheet(
            "QWidget#login-area{ border: 2px solid black; background-color: white}")
        loginArea.setLayout(self._createLoginForm())

        return loginArea

    def _createLoginForm(self):
        layout = QGridLayout()

        login = QLabel('Login:')
        login.setFont(QFont('Segoe UI', 9))
        self.line_log = QLineEdit()
        login_validator = QRegExpValidator(QRegExp('[^;\ \-\'"]*'))
        self.line_log.setValidator(login_validator)
        self.line_log.setPlaceholderText('Wpisz login')
        layout.addWidget(login, 0, 0)
        layout.addWidget(self.line_log, 0, 1)

        password = QLabel('Hasło:')
        password.setFont(QFont('Segoe UI', 9))
        self.line_passwd = QLineEdit()
        self.line_passwd.setPlaceholderText('Wpisz hasło')
        self.line_passwd.setEchoMode(QLineEdit.Password)
        self.line_passwd.setValidator(login_validator)
        layout.addWidget(password, 1, 0)
        layout.addWidget(self.line_passwd, 1, 1)

        self.buttons = {}
        login_button = QPushButton('Zaloguj')
        login_button.setDefault(True)
        self.buttons['login'] = login_button
        # login_button.clicked.connect(self.check_passwd)
        layout.addWidget(login_button, 2, 0, 1, 2)

        return layout