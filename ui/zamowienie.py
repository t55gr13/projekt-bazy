import PyQt5.QtWidgets as qt
import PyQt5.QtCore as qtcore


class OknoZamowienia(qt.QWidget):

    def __init__(self):
        """Create menu UI.
        """
        super().__init__()

        self.buttons = {}

    def createLayoutZ(self, db):
        """Create menu UI layout

        Args:
            db (datbase.Database): connection to database

        Returns:
            QGridLayout: interface layout
        """
        self.layout = qt.QGridLayout()
        self.setLayout(self.layout)

        self.zamowienie = self._createZamowieniearea(db)
        self.zamowienie.setStyleSheet("background-color: #ffcce6")

        self.button_bar = self._createButtonBar()
        self.button_bar.setStyleSheet("background-color: #a4a4c1")

        self.layout.addWidget(self.zamowienie, 1, 0, 11, 5)
        self.layout.addWidget(self.button_bar, 0, 0, 1, 5)
        self.layout.setContentsMargins(qtcore.QMargins(0, 0, 0, 0))
        self.layout.setSpacing(0)

        return self.layout

    def _createZamowieniearea(self, zamowienie_items):

        zamowieniearea = qt.QTabWidget()
        zamowieniearea.addTab(self._createZamowienieLayout(zamowienie_items), 'Zamówienia')


        return zamowieniearea
    
    def addTab(self, items, name):
        self.zamowienie.addTab(self._createZamowienieLayout(items), name)

    def _createZamowienieLayout(self, zamowienie_items):

        dane = qt.QWidget()
        dane_layout = qt.QGridLayout()
        blank = qt.QWidget()
        blank.setFixedHeight(1)
        dane_layout.addWidget(blank, 0, 0, 1, 2)
        dane_layout.setAlignment(qtcore.Qt.AlignTop )
        dane.setLayout(dane_layout)


        for i in range(len(zamowienie_items)):

            dane_layout.addWidget(zamowienie_items[i], (i//2)+1, i % 2)

        scrollarea = qt.QScrollArea()
        scrollarea.setWidgetResizable(True)
        scrollarea.setWidget(dane)

        return scrollarea

    def _createButtonBar(self):
        """[summary]

        Returns:
            QWidget: Order area widget
        """
        buttonbar = qt.QWidget()
        layout = qt.QGridLayout()
        buttonbar.setLayout(layout)

        self.buttons['go_back'] = qt.QPushButton('wróć')
        self.buttons['go_back'].setStyleSheet("background-color: white;")
        self.buttons['go_back'].setMaximumWidth(100)

        layout.addWidget(self.buttons['go_back'])

        return buttonbar