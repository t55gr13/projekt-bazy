import PyQt5.QtWidgets as qt
from PyQt5.QtGui import *
import PyQt5.QtCore as qtcore
import decimal


class OrderItem(qt.QWidget):
    """Item that is placed in order area.
    Holds information about product in order. Allow to modify amount of it in order
    or delet it.
    """

    def __init__(self, id, label, price, amount=1, colors={}):
        """Create order item

        Args:
            label (string): product name
            price (string): product price
        """
        super().__init__()

        self._id = id
        self._label = label
        self._price = price
        self._amount = amount
        self.color = colors

        # Put content widget to item in layout
        self._content = self._createItemContent(label, price)

        content_layout = qt.QBoxLayout(qt.QBoxLayout.BottomToTop)
        content_layout.addWidget(self._content)
        # margin of single item
        content_layout.setContentsMargins(qtcore.QMargins(5, 5, 5, 5))
        content_layout.setSpacing(5)
        self.setLayout(content_layout)

        # Minimal item height
        # self.setMinimumHeight(100)
        self.setFixedHeight(80)

    def _createItemContent(self, label, price):
        """Create widget with item content

        Args:
            label (string): product name
            price (string): product price

        Returns:
            QWidget: content widget
        """
        content = qt.QWidget(self)
        item_style = f"""
        QWidget {{
            background-color: {self.color['secondary']};
            color: {self.color['primaryText']}; 
            border: none;
        }}
        QPushButton {{
            background-color: {self.color['secondaryDark']};
            color: {self.color['primaryText']}; 
            border: none;
        }}
        QPushButton:hover {{ 
            background-color: {self.color['secondaryLight']}; 
        }}
        QPushButton:pressed {{ 
            background-color: {self.color['secondary']}; 
        }}
        """
        content.setStyleSheet(item_style)
        # Product name
        lab = qt.QLabel(label)
        # lab.setAlignment(qtcore.Qt.AlignCenter)
        # lab.setWordWrap(True)
        # lab.setStyleSheet(
        #     "font-size: 16px; border-width: 1px; border-color: black; border-style: solid;")
        amount = qt.QLabel('x'+str(self._amount))
        # amount.setStyleSheet(
        #     "font-size: 16px; border-width: 1px; border-color: black; border-style: solid;")
        # Product price
        pr = qt.QLabel(str(int(self._amount)*decimal.Decimal(price)))
        # pr.setAlignment(qtcore.Qt.AlignRight | qtcore.Qt.AlignVCenter)
        # pr.setStyleSheet(
        #     "border-width: 1px; border-color: black; border-style: solid;")

        buttons = qt.QWidget()
        btn_layout = qt.QGridLayout()
        btn_layout.setContentsMargins(qtcore.QMargins(1, 1, 1, 1))
        # buttons.setStyleSheet("background-color: green; border-width: 1px; border-color: black; border-style: solid;")

        self.buttons = {}
        self.buttons['incr'] = qt.QPushButton('+')
        self.buttons['decr'] = qt.QPushButton('-')
        self.buttons['remove'] = qt.QPushButton('X')
        # self.buttons['incr'].setStyleSheet("background-color: aqua; border-width: 1px; border-color: black; border-style: solid;")
        # self.buttons['decr'].setStyleSheet("background-color: aqua; border-width: 1px; border-color: black; border-style: solid;")
        # self.buttons['remove'].setStyleSheet("background-color: aqua; border-width: 1px; border-color: black; border-style: solid;")
        btn_layout.addWidget(self.buttons['incr'], 0, 0)
        btn_layout.addWidget(self.buttons['decr'], 1, 0)
        btn_layout.addWidget(self.buttons['remove'], 0, 1)
        buttons.setLayout(btn_layout)


        # Create content layout and put labels on it
        l = qt.QHBoxLayout()
        l.setSpacing(10)
        l.addWidget(lab, 10)
        l.addWidget(amount, 2)
        l.addWidget(pr, 2)
        l.addWidget(buttons, 3)
        # l.setSpacing(0)
        # Set layout to content widget
        content.setLayout(l)

        return content

    def increment(self):
        self._amount += 1
        self._content.layout().itemAt(1).widget().setText('x'+
            str(self._amount))
        self._content.layout().itemAt(2).widget().setText(
            str(int(self._amount)*decimal.Decimal(self._price)))
    
    def decrement(self):
        self._amount -= 1
        self._content.layout().itemAt(1).widget().setText('x'+
            str(self._amount))
        self._content.layout().itemAt(2).widget().setText(
            str(int(self._amount)*decimal.Decimal(self._price)))
