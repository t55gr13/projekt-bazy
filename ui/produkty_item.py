import PyQt5.QtWidgets as qt
import PyQt5.QtCore as qtcore


class IngredientItem(qt.QWidget):

    def __init__(self, id, name, sum, unit, min_level):

        super().__init__()

        self._id = id
        self._name = name
        self._sum = sum
        self._unit = unit
        self._min_level = min_level

        content_layout = qt.QBoxLayout(qt.QBoxLayout.BottomToTop)
        content_layout.addWidget(self._createItemContent())

        self.setLayout(content_layout)


    def _createItemContent(self):

        content = qt.QWidget(self)

        name = qt.QLabel(f'<b>[{self._name}]</b>')
        name.setAlignment(qtcore.Qt.AlignLeft)

        amount = qt.QLabel(f'{self._sum} {self._unit}')
        amount.setAlignment(qtcore.Qt.AlignLeft)

        min_level = qt.QLabel(f'<b>Min. poziom:</b> {self._min_level} {self._unit}')
        min_level.setAlignment(qtcore.Qt.AlignLeft)

        l = qt.QGridLayout()
        l.addWidget(name, 0, 0, 1, 5)
        l.addWidget(amount, 1, 0, 1, 2)
        l.addWidget(min_level, 1, 2, 1, 1)


        content.setLayout(l)
        if float(self._sum) <= float(self._min_level):
            print(self._name, self._sum, self._min_level)
            self.setStyleSheet("background-color: red;")
        else:
            self.setStyleSheet("background-color: #ff99ce;")

        return content

    def setClickedHandle(self, handle_func):
        self._clickedHandle = handle_func

    def mousePressEvent(self, event):
        """Function called when IngredientItem is pressed

        Args:
            event (QEvent (?)): Qt event object
        """
        super().mousePressEvent(event)

        print('clicked {}'.format(self._name))
        # call clicked item handle function
        self._clickedHandle(self._id)