import database


class User():
    """Object that describes user of the application.
    contains personal information, priviledges and other.
    """

    def __init__(self, id, name, surname, tel, admin, login, passwd):
        self._id = id
        self._name = name
        self._surname = surname
        self._tel = tel
        self._admin = admin
        self._login = login

        db_settings = {
            "user": "bazy."+self._login,
            "password": passwd,
            "db-name": "pizzeria",
            "host": "localhost"
        }

        # Create db connection as user
        self._db = database.Database(db_settings)
        self._db.connect()
