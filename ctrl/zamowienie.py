import ctrl
import ui
from datetime import datetime

class ZamowienieCtrl(ctrl.Controller):

    def __init__(self, app, ui):
        super().__init__(app, ui)
        self.setup()

    def setup(self):

        self._ui.createLayoutZ(self.getZamowienieItems())
        self._ui.addTab(self.getZamowienieDzisiajItems(), 'Zamówienia dzisiaj')
        self._ui.buttons['go_back'].clicked.connect(self.go_backHandle)

        # Set style settings from 'app.conf'
        self._ui.setStyleSheet(f'font-size: {self._app._settings["ui"]["menu-font-size"]};')

    def getZamowienieItems(self):

        q = self._app._user._db.query(
            'select idzamowienia, czaszlozenia, czasplatnosci, idTypu, wartoscrachunku, idpracownika from zamowienia;')

        items = []

        for row in q:
            zamowienia_item = ui.zamowienia_item.ZamowieniaItem(
                str((row[0])), str(row[1]), str(row[2]), str((row[3])), str((row[4])), str((row[5]))
            )
            items.append(zamowienia_item)

        return items

    def getZamowienieDzisiajItems(self):

        q = self._app._user._db.query(
            'select idzamowienia, czaszlozenia, czasplatnosci, idTypu, wartoscrachunku, idpracownika from zamowienia_dzisiaj;')

        items = []

        for row in q:
            zamowienia_item = ui.zamowienia_item.ZamowieniaItem(
                str((row[0])), str(row[1]), str(row[2]), str((row[3])), str((row[4])), str((row[5]))
            )
            items.append(zamowienia_item)

        return items

    def go_backHandle(self):
        self._app.setUI('main_menu')
