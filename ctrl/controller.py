import PyQt5.QtWidgets as qt
import core
import ui


class Controller():
    """App controller class
    """

    def __init__(self, app, ui):
        """Create Controller object

        Args:
            app (App): handle to App object
            ui (QWidget): handle to ui object
        """
        # Set handles for app and ui objects
        self._app = app
        self._ui = ui

    def setup(self):
        """Configure UI with application
        """
        raise NotImplementedError("Controller is abstract class.")
