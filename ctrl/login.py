import PyQt5.QtWidgets as qt
import core
import ui
import ctrl
import user
import database
# import controller


class LoginCtrl(ctrl.Controller):
    """Login UI Controller Class
    """

    def __init__(self, app, ui):
        """Create LoginCtrl object

        Args:
            app (App): handle to App object
            ui (QWidget): handle to ui object
        """
        super().__init__(app, ui)
        self.setup()

    def setup(self):
        """Configure UI with application
        """
        # Connect functions to the buttons
        # When enter is pressed whe editing password line
        self._ui.line_log.returnPressed.connect(self._loginHandle)
        self._ui.line_passwd.returnPressed.connect(self._loginHandle)
        # When login button is pressed
        self._ui.buttons['login'].clicked.connect(self._loginHandle)

    def _loginHandle(self):
        """Login into application
        """

        # Create login message box
        mess = qt.QMessageBox()
        mess.setWindowTitle('Komunikat')
        # try to log in
        if self._app.checkPassword(self._ui.line_log.text(), self._ui.line_passwd.text()) is True:
            # Create user
            q = self._app._db.query(f"select * from pracownicy where login='{self._ui.line_log.text()}';")
            # print(q)
            self._app._user = user.User(q[0][0], q[0][1], q[0][2], q[0][3], q[0][4], q[0][5], self._ui.line_passwd.text())

            # Show message
            mess.setText('Logowanie zakończone powodzeniem!')
            mess.exec_()
            # Switch to main menu UI
            self._app.setUI('main_menu')
        else:
            # Show message
            mess.setText('Wprowadzone dane są niepoprawne!')
            mess.exec_()
