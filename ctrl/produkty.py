import ctrl
import ui
from datetime import datetime
from functools import partial

import PyQt5.QtWidgets as qt
import PyQt5.QtCore as qtcore
import PyQt5.QtGui as qtgui


class ProduktyCtrl(ctrl.Controller):

    def __init__(self, app, ui):
        super().__init__(app, ui)
        self.setup()

    def setup(self):

        self._ui.createLayoutP(self.getIngredeintItems())
        self._ui.buttons['go_back'].clicked.connect(self.go_backHandle)
        self._ui.buttons['new_ingr'].clicked.connect(self.new_ingrHandle)
        self._ui.buttons['update_ingr'].clicked.connect(self.update_ingrHandle)
        self._ui.buttons['shopping-list'].clicked.connect(self.openShoppingList)

        if self._app._user._admin == False:
            self._ui.buttons['new_ingr'].hide()
            self._ui.buttons['update_ingr'].hide()

        # Set style settings from 'app.conf'
        self._ui.setStyleSheet(f'font-size: {self._app._settings["ui"]["menu-font-size"]};')

    def getIngredeintItems(self):

        q = self._app._user._db.query(
            'select * from skladniki;')

        items = []

        for row in q:
            item = ui.produkty_item.IngredientItem(
                str((row[0])), str(row[1]), str(row[2]), str((row[3])), str((row[4])))
            item.setClickedHandle(self.ingredientClicked)
            # item
            items.append(item)

        return items

    def go_backHandle(self):
        self._app.setUI('main_menu')

    def new_ingrHandle(self):
        self.dialog = self.createNewIngredientWidget()
        widget_width = self._app._settings['window']['width']//3
        widget_height = self._app._settings['window']['height']//5
        self.dialog.setMinimumWidth(widget_width)
        self.dialog.setMinimumHeight(widget_height)
        self.dialog.show()

    def update_ingrHandle(self):
        self.dialog = self.createUpdateIngredientWidget()
        self.dialog.layout().itemAtPosition(0, 0).widget().currentTextChanged.connect(self.changeIngrId)
        self.changeIngrId()
        widget_width = self._app._settings['window']['width']//3
        widget_height = self._app._settings['window']['height']//5
        self.dialog.setMinimumWidth(widget_width)
        self.dialog.setMinimumHeight(widget_height)
        self.dialog.show()

    def ingredientClicked(self, ingr_id):
        if self._app._user._admin == False:
            return
            
        self.dialog = self.createIngredientWidget(ingr_id)
        widget_width = self._app._settings['window']['width']//3
        widget_height = self._app._settings['window']['height']//5
        self.dialog.setMinimumWidth(widget_width)
        self.dialog.setMinimumHeight(widget_height)
        self.dialog.show()

    def createIngredientWidget(self, ingr_id):
        widget = qt.QDialog()
        widget.setWindowTitle("składnik")
        layout = qt.QGridLayout()
        widget.setLayout(layout)

        q = self._app._user._db.query(f"select * from skladniki where idskladnika={ingr_id};")

        name_label = qt.QLabel(f"<b>nazwa:</b>")
        name = qt.QLineEdit(str(q[0][1]))
        name.setValidator(qtgui.QRegExpValidator(qtcore.QRegExp('[^;\ \-\'"]*')))
        amount_label = qt.QLabel(f"<b>ilość:</b>")
        amount = qt.QLineEdit(str(q[0][2]).replace('.', ','))
        amount.setValidator(qtgui.QDoubleValidator(0.0, 100000.0, 4, widget))
        unit = qt.QLineEdit(str(q[0][3]))
        unit.setValidator(qtgui.QRegExpValidator(qtcore.QRegExp('[^;\ \-\'"]*')))
        min_level_label = qt.QLabel(f"<b>poz. min.:</b>")
        min_level = qt.QLineEdit(str(q[0][4]).replace('.', ','))
        min_level.setValidator(qtgui.QDoubleValidator(0.0, 100000.0, 4, widget))
        zmodyfikuj = qt.QPushButton('Zmodyfikuj')
        zmodyfikuj.clicked.connect(partial(self.modifyIngredient, ingr_id))
        usun = qt.QPushButton('Usuń')
        usun.clicked.connect(partial(self.deleteIngredient, ingr_id))
        if self._app._user._admin == False:
            zmodyfikuj.hide()
            usun.hide()
        

        layout.addWidget(name_label, 0, 0, 1, 2)
        layout.addWidget(name, 0, 2, 1, 5)
        layout.addWidget(zmodyfikuj, 0, 7, 1, 1)
        layout.addWidget(usun, 2, 7, 1, 1)
        layout.addWidget(amount_label, 1, 0, 1, 2)
        layout.addWidget(min_level_label, 2, 0, 1, 2)
        layout.addWidget(amount, 1, 2, 1, 5)
        layout.addWidget(unit, 1, 7, 1, 1)
        layout.addWidget(min_level, 2, 2, 1, 5)

        return widget

    def createNewIngredientWidget(self):
        widget = qt.QDialog()
        widget.setWindowTitle("składnik")
        layout = qt.QGridLayout()
        widget.setLayout(layout)


        name_label = qt.QLabel(f"<b>nazwa:</b>")
        name = qt.QLineEdit()
        name.setValidator(qtgui.QRegExpValidator(qtcore.QRegExp('[^;\ \-\'"]*')))
        unit_label = qt.QLabel(f"<b>jedn.:</b>")
        unit = qt.QLineEdit()
        unit.setValidator(qtgui.QRegExpValidator(qtcore.QRegExp('[^;\ \-\'"]*')))
        min_level_label = qt.QLabel(f"<b>poz. min.:</b>")
        min_level = qt.QLineEdit()
        min_level.setValidator(qtgui.QDoubleValidator(0.0, 100000.0, 4, widget))
        dodaj = qt.QPushButton('dodaj')
        dodaj.clicked.connect(self.newIngredient)

        layout.addWidget(name_label, 0, 0, 1, 2)
        layout.addWidget(name, 0, 2, 1, 5)
        layout.addWidget(dodaj, 0, 7, 1, 1)
        layout.addWidget(unit_label, 1, 0, 1, 2)
        layout.addWidget(unit, 1, 2, 1, 5)
        layout.addWidget(min_level_label, 2, 0, 1, 2)
        layout.addWidget(min_level, 2, 2, 1, 5)

        return widget

    def createUpdateIngredientWidget(self):
        widget = qt.QDialog()
        widget.setWindowTitle("składnik")
        layout = qt.QGridLayout()
        widget.setLayout(layout)

        q = self._app._user._db.query(f"select * from skladniki;")

        self.ingr_id = 0
        ingr = qt.QComboBox()
        # ingr.currentTextChanged.connect(self.changeIngrId)
        for item in q:
            ingr.addItem(item[1])

        # name.setStyleSheet("font-size: 18px;")
        amount_label = qt.QLabel(f"<b>ilość:</b>")
        amount = qt.QLineEdit('')
        amount.setValidator(qtgui.QDoubleValidator(0.0, 100000.0, 4, widget))
        unit = qt.QLabel('')
        zmodyfikuj = qt.QPushButton('Dodaj')
        zmodyfikuj.clicked.connect(self.updateIngredient)

        # layout.addWidget(name_label, 0, 0, 1, 2)
        layout.addWidget(ingr, 0, 0, 1, 5)
        layout.addWidget(zmodyfikuj, 0, 7, 1, 1)
        layout.addWidget(amount_label, 1, 0, 1, 2)
        layout.addWidget(amount, 1, 2, 1, 5)
        layout.addWidget(unit, 1, 7, 1, 1)

        return widget

    def changeIngrId(self):
        ingr = self.dialog.layout().itemAtPosition(0, 0).widget().currentText()
        q = self._app._user._db.query(f"select idskladnika, jednostka from skladniki where nazwa='{ingr}';")
        self.ingr_id = q[0][0]
        ingr = self.dialog.layout().itemAtPosition(1, 7).widget().setText(q[0][1])


    def modifyIngredient(self, ingr_id):
        name = self.dialog.layout().itemAtPosition(0, 2).widget().text()
        sum = self.dialog.layout().itemAtPosition(1, 2).widget().text().replace(',', '.')
        unit = self.dialog.layout().itemAtPosition(1, 7).widget().text()
        minimal = self.dialog.layout().itemAtPosition(2, 2).widget().text().replace(',', '.')
        self._app._user._db.update(f"update skladniki set nazwa='{name}', suma={sum}, jednostka='{unit}', poziomminimalny={minimal} where idskladnika={ingr_id}")

        self.updateIngredientWidget()
        self.dialog.hide()
        self.dialog.setParent(None)
        self.dialog = None

    def updateIngredient(self):
        # ingr= self.dialog.layout().itemAtPosition(0, 0).widget().currentText()
        amount = self._app._user._db.query(f"select suma from skladniki where idskladnika={self.ingr_id};")[0][0]
        update_amount = self.dialog.layout().itemAtPosition(1, 2).widget().text().replace(',', '.')
        self._app._user._db.update(f"update skladniki set suma={float(amount)+float(update_amount)} where idskladnika={self.ingr_id}")

        self.updateIngredientWidget()
        self.dialog.hide()
        self.dialog.setParent(None)
        self.dialog = None

    def newIngredient(self):
        name = self.dialog.layout().itemAtPosition(0, 2).widget().text()
        sum = '0.0'
        unit = self.dialog.layout().itemAtPosition(1, 2).widget().text()
        minimal = self.dialog.layout().itemAtPosition(2, 2).widget().text().replace(',', '.')
        self._app._user._db.insert("insert into skladniki (nazwa, suma, jednostka, poziomminimalny) values (%s,%s,%s,%s)", [(name, sum, unit, minimal,)])

        self.updateIngredientWidget()
        self.dialog.hide()
        self.dialog.setParent(None)
        self.dialog = None

    def deleteIngredient(self, ingr_id):
        # amount = self._app._user._db.query(f"select suma from skladniki where idskladnika={ingr_id};")[0][0]
        # update_amount = self.dialog.layout().itemAtPosition(1, 2).widget().text().replace(',', '.')
        self._app._user._db.delete(f"delete from skladniki where idskladnika={ingr_id}")

        self.updateIngredientWidget()
        self.dialog.hide()
        self.dialog.setParent(None)
        self.dialog = None

    def updateIngredientWidget(self):
        ingr_widget = self._ui._createIngredientsArea(self.getIngredeintItems())
        ingr_widget.setStyleSheet("background-color: #ffcce6")


        self._ui.layout.itemAtPosition(1, 0).widget().setParent(None)
        self._ui.layout.addWidget(ingr_widget, 1, 0, 11, 5)

    def openShoppingList(self):
        

        widget = qt.QDialog()
        widget.setWindowTitle('Lista zakupów')
        layout = qt.QGridLayout()

        list_widget = qt.QListWidget()
        monospace_font = qtgui.QFontDatabase.systemFont(qtgui.QFontDatabase.FixedFont)
        list_widget.setFont(monospace_font)
        q = self._app._user._db.query(f'select nazwa, suma, jednostka, poziomminimalny from skladniki where suma<poziomminimalny;')

        for item in q:
            val = float(item[3]) - float(item[1])
            list_item_text = f"{item[0]:<25} {val:>6.3} {item[2]:<6}"
            list_item = qt.QListWidgetItem(list_item_text)
            list_widget.addItem(list_item)

        export_button = qt.QPushButton('Eksportuj\ndo pliku')
        # list_widget.itemDoubleClicked.connect()
        export_button.clicked.connect(self.generateListFile)

        labels = qt.QLabel(f"{'składnik':<25}{'do min. poziomu':^12}")
        labels.setFont(monospace_font)

        layout.addWidget(labels, 0, 0, 1, 3)
        layout.addWidget(list_widget, 1, 0, 5, 3)
        layout.addWidget(export_button, 6, 1, 1, 1)

        widget.setLayout(layout)
        self.dialog = widget

        widget_width = self._app._settings['window']['width']//3
        widget_height = self._app._settings['window']['height']//4
        self.dialog.setMinimumWidth(widget_width)
        self.dialog.setMinimumHeight(widget_height)
        self.dialog.show()

    def generateListFile(self):
        q = self._app._user._db.query(f'select nazwa, suma, jednostka, poziomminimalny from skladniki where suma<poziomminimalny;')

        with open('shopping_list.txt', 'w', encoding='utf-8') as file:
            file.write(f"{'składnik':<25}{'do min. poziomu':^12}\n")
            file.write('-'*40+'\n')
            for item in q:
                val = float(item[3]) - float(item[1])
                line = f"{item[0]:<25} {val:>6.3} {item[2]:<6}\n"
                file.write(line)
            file.write('-'*40+'\n')
        
        self.dialog.hide()
        self.dialog.setParent(None)
        self.dialog = None
            