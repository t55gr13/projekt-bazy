import json


def load_json(path):
    with open(path, "r") as f:
        return json.load(f)


config = load_json('app.conf')
color = config['ui']['color']
style = {}


style['edit-menu-on'] = f"""
QPushButton {{
    background-color: {color['primaryLight']};
    color: {color['primaryText']};
    border-style: solid;
    border-width: 3px;
    border-radius: 20px;
    border-color: {color['primary']};
    width: 40px;
    height: 40px;
}}
QPushButton:focus {{
    outline: none;
}}
"""

style['edit-menu-off'] = f"""
QPushButton {{
    background-color: {color['primary']};
    color: {color['primaryText']};
    border-style: solid;
    border-width: 3px;
    border-radius: 20px;
    border-color: {color['primary']};
    width: 40px;
    height: 40px;
}}
QPushButton:focus {{
    outline: none;
}}
QPushButton:hover {{
    background-color: {color['primaryLight']};
}}
"""


style['order-area'] = f"""
QWidget {{
    background-color: {color['primary']};
    color: {color['primaryText']}; 
}}
QPushButton {{
    background-color: {color['secondary']};
    color: {color['primaryText']}; 
    border: none;
    height: 30px;
}}
QPushButton:hover {{ 
    background-color: {color['secondaryLight']}; 
}}
QPushButton:pressed {{ 
    background-color: {color['secondary']}; 
}}
"""

style['menu-area'] = f"""
        QWidget {{
            background-color: {color['surface']};
            color: {color['primaryText']}; 
            border: none;
        }}
        QPushButton {{
            background-color: {color['secondaryDark']};
            color: {color['primaryText']}; 
            border: none;
        }}
        QPushButton:hover {{ 
            background-color: {color['secondaryLight']}; 
        }}
        QPushButton:pressed {{ 
            background-color: {color['secondary']}; 
        }}
        """


style['order'] = f"""
        QWidget {{
            background-color: {color['surface']};
            color: {color['primaryText']}; 
            border: none;
        }}
        QPushButton {{
            background-color: {color['secondary']};
            color: {color['primaryText']}; 
            border: none;
            height: 30px;
        }}
        QPushButton:hover {{ 
            background-color: {color['secondaryLight']}; 
        }}
        QPushButton:pressed {{ 
            background-color: {color['secondary']}; 
        }}
        """

style['menu-item'] = f"""
QWidget {{
    background-color: {color['secondary']};
    color: {color['primaryText']}; 
    border: none;
}}
QWidget:hover {{ 
    background-color: {color['secondaryLight']}; 
}}
QWidget > QLabel {{ 
    background-color: {color['secondary']};
}}
QWidget:pressed {{ 
    background-color: {color['secondary']}; 
}}
QPushButton {{
    background-color: {color['secondaryDark']};
    border-style: solid;
    border-width: 1px;
    border-color: {color['secondaryDark']};
}}
QPushButton:hover {{
    background-color: {color['secondary']};
}}
QPushButton:pressed {{
    background-color: {color['secondaryDark']};
}}
"""

style['menu-button'] = f"""
QPushButton {{
    background-color: {color['primary']};
    color: {color['primaryText']}; 
    border: none;
}}
QPushButton:hover {{ 
    background-color: {color['primaryLight']}; 
}}
QPushButton:pressed {{ 
    background-color: {color['primary']}; 
}}
"""