
:: Install requirements
@echo install Python packages
@pip install -r requirements.txt

:: Create app as one exe file from python code
@echo install app into dist/ direcory
@pyinstaller --noconfirm --onefile --windowed --add-data "./core.py;." --add-data "./database.py;." --add-data "./user.py;." --add-data "./ctrl;ctrl/" --add-data "./ui;ui/"  "./pizzeria.py"
@copy app.conf dist\app.conf

:: Create app as folder with exe file from python code
:: with console for debbuging purposes
:: pyinstaller --noconfirm --onedirectory --console --add-data "./core.py;." --add-data "./database.py;." --add-data "./user.py;." --add-data "./ctrl;ctrl/" --add-data "./ui;ui/"  "./main.py"
